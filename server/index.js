// This RPC server will announce itself as `rpc_test`
// in our Grape Bittorrent network
// When it receives requests, it will answer with 'world'

'use strict'
const crypto = require('crypto')
const { PeerRPCServer } = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')

const link = new Link({
  grape: 'http://127.0.0.1:30001'
})
link.start()

const peer = new PeerRPCServer(link, {
  timeout: 300000
})
peer.init()

// type: 1 === buy
// type: 2 === sell
const orderBook = new Map()

const port = 1337 // + Math.floor(Math.random() * 1000)
const service = peer.transport('server')
service.listen(port)

setInterval(function () {
  link.announce('rpc_test', service.port, {})
  link.announce('create_new_order', service.port, {})
  link.announce('get_open_orders', service.port, {})
}, 1000)

service.on('request', (rid, key, payload, handler) => {
  switch (key) {
    case 'create_new_order':
      orderBook.set(hashme(), payload)
      handler.reply(null, 'Order saved')
      break
    case 'get_open_orders':
      console.log(payload, orderBook)
      const orders = {}
      for (const [key, value] of orderBook.entries()) {
        if (value.customer !== payload.customer) {
          orders[key] = {
            ...value,
            key,
          }
        }
      }
      handler.reply(null, orders)
      break
    default:
      handler.reply(null, 'Connection successful')
  }
})

function hashme(size = 10) {
  return crypto
    .randomBytes(size)
    .toString('base64')
    .slice(0, size)
}