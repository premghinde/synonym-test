// This client will as the DHT for a service called `rpc_test`
// and then establishes a P2P connection it.
// It will then send { msg: 'hello' } to the RPC server

'use strict'

const crypto = require('crypto')
const readline = require('readline')
const events = require('events')

const { PeerRPCClient } = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')

class Events extends events { }
const e = new Events()
const sessionHash = crypto
  .randomBytes(10)
  .toString('base64')
  .slice(0, 10)

const link = new Link({
  grape: 'http://127.0.0.1:30001'
})
link.start()

const peer = new PeerRPCClient(link, {})
peer.init()

// create 5 random orders on the server for this client instance
for (let i = 0; i < 5; i++) {
  const order = {
    quantity: Math.floor(Math.random() * 1000),
    price: Math.floor(Math.random() * 69000),
    type: Math.floor((Math.random()) * 2) + 1,
    customer: sessionHash,
  }
  peer.request('create_new_order', order, { timeout: 10000 }, (err, data) => {
    if (err) {
      console.error(err)
      process.exit(-1)
    }
  })
}

const prompt = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: '>> '
})

const verify = () => peer.request('rpc_test', { msg: 'hello' }, { timeout: 10000 }, (err, data) => {
  if (err) {
    console.error(err)
    process.exit(-1)
  }
  console.log(data) // { msg: 'world' }
  prompt.prompt()
})

const createNewOrder = () => {
  const order = {
    customer: sessionHash
  }
  prompt.question('Quantity? >> ', quantity => {
    order.quantity = Number(quantity)
    prompt.question('Price? >> ', price => {
      order.price = Number(price)
      prompt.question('Buy/Sell? >> ', direction => {
        if (direction.toLowerCase() === 'buy') {
          order.type = 1
        }
        if (direction.toLowerCase() === 'sell') {
          order.type = 2
        }
        peer.request('create_new_order', order, { timeout: 10000 }, (err, data) => {
          if (err) {
            console.error(err)
            process.exit(-1)
          }
          console.log(data)
          prompt.prompt()
        })
      })
    })
  })
}

const getOpenOrders = () => {
  peer.request('get_open_orders', { customer: sessionHash }, { timeout: 10000 }, (err, data) => {
    if (err) {
      console.error(err)
      process.exit(-1)
    }
    const keys = Object.keys(data)
    const buys = []
    const sells = []
    if (keys.length) {
      Object.keys(data).forEach(key => {
        if (data[key].type === 1) {
          buys.push(data[key])
        }
        if (data[key].type === 2) {
          sells.push(data[key])
        }
      })
      if (buys.length) {
        console.log('---- BUY ORDERS ----')
        buys
          .sort((a, b) => a.price < b.price ? 1 : -1)
          .forEach(order => console.log({
            quantity: order.quantity,
            price: order.price,
            orderID: order.key,
          }))
      }
      if (sells.length) {
        console.log('--- SELL ORDERS ----')
        sells
          .sort((a, b) => a.price > b.price ? 1 : -1)
          .forEach(order => console.log({
            quantity: order.quantity,
            price: order.price,
            orderID: order.key,
          }))
      }
    } else {
      console.log('Order book is currently empty')
    }
    prompt.prompt()
  })
}

const cli = {
  commands: [
    'get open orders',
    'create new order',
    'verify',
  ], init: () => {
    console.log('\x1b[34m%s\x1b[0m', `CLI up and running\nCLI Commands:`)
    cli.commands.forEach((input, i) => {
      console.log('\x1b[34m%s\x1b[0m', `${++i}) ${input}`)
    })

    prompt.prompt()

    prompt.on('line', str => {
      cli.input(str)
    })

    e.on('create new order', createNewOrder)
    e.on('get open orders', getOpenOrders)
    e.on('verify', verify)
  },
  input: str => {
    str = str.trim().length > 0 ? str : false

    if (str) {
      let matched = false
      cli.commands.some(input => {
        if (str.toLowerCase().indexOf(input) > -1) {
          matched = true
          e.emit(input, str)
          return true
        }
      })

      if (!matched) {
        console.log('Bad command. Try again.')
      }
    }
  }
}

cli.init()

module.exports = cli
