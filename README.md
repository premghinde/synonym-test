# Synonym Interview Test #

### What is this repository for? ###

* Demonstration of Grenache implementation
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

`npm install` to install dependencies required for this project

You need 2 terminal windows to execute a distributed Grenache setup

1. `grape --dp 20001 --aph 30001 --bn '127.0.0.1:20002'`
2. `grape --dp 20002 --aph 40001 --bn '127.0.0.1:20001'`

You need another to start up a server instance that will connect to a Grenache instance

1. `node server`

You can then set up as many clients as you wish

1. `node client`

### How do I use it? ###

You interact with the client through a CLI interface which will be swapped out for a web interface. The command you have are:

- `verify` - does a ping to the server to prove basic connectivity
- `create new order` - will ask you a series of questions to collect data for your buy or sell order and then sends it to the server
- `get open orders` - retrieves all current orders registered with the server that do not belong to this session/customer

### Further thoughts ###
- Creating a settle order call to allow Customer A to settle an order of Customer X by sending the hash of the order to the server. Getting the two users connected and creating channels for direct communnication is both an unknown and probably a rather large piece of work.
